# frozen_string_literal: true

Rails.application.routes.draw do
  root 'welcome#index'
  get 'welcome', to: 'welcome#welcome'

  devise_for :users,
             defaults: { format: :json },
             class_name: 'Api::V1::User',
             controllers: {
               sessions: 'api/v1/sessions',
               registrations: 'api/v1/registrations'
             },
             path_prefix: '/api/v1'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :users do
        collection do
          get :current
        end
      end
      resources :books
      resources :authors

      get 'system_version', to: 'system_version#show'
    end
  end

  # Rails will pass anything it does not match to index.html so that react-router can take over.
  get '*path', to: 'application#fallback_index_html', constraints: lambda { |request|
    !request.xhr? && request.format.html?
  }
end
