require 'rails_helper'

RSpec.describe Api::V1::AuthorsController, type: :request do
  let!(:author) { create :author }
  let(:author_params) { attributes_for(:author) }
  let(:params) { { author: author_params } }

  describe 'GET #index' do
    subject { get api_v1_authors_path }

    it 'returns a success response' do
      subject
      expect(response).to be_successful
      expect(response_json.first.keys)
        .to contain_exactly(
          'id', 'first_name', 'last_name', 'count_of_books', 'created_at', 'photo_attached?',
          'photo_filename', 'photo_url', 'updated_at', 'url'
        )
    end
  end

  describe 'GET #show' do
    subject { get api_v1_author_path(author.id) }

    it 'returns a success response' do
      subject

      expect(response).to be_successful
      expect(response_json.keys)
        .to contain_exactly(
          'id', 'first_name', 'last_name', 'count_of_books', 'created_at', 'photo_attached?',
          'photo_filename', 'photo_url', 'updated_at'
        )
    end
  end

  describe 'POST #create' do
    subject { post api_v1_authors_path, params: params }

    context 'with valid params' do
      it 'creates a new author' do
        expect { subject }.to change(Api::V1::Author, :count).by(1)
      end

      it 'renders a JSON response with the new author' do
        subject

        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(response_json.keys)
          .to contain_exactly(
            'id', 'first_name', 'last_name', 'count_of_books', 'created_at', 'photo_attached?',
            'photo_filename', 'photo_url', 'updated_at'
          )
      end
    end

    context 'with invalid params' do
      let(:author_params) { { title: '' } }

      it 'renders a JSON response with errors for the new author' do
        subject

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end
  end

  describe 'PUT #update' do
    let(:new_first_name) { 'Completely new first name' }
    let(:author_params) { { first_name: new_first_name } }
    subject { put api_v1_author_path(author.id), params: params }

    before :each do
      subject
    end

    context 'with valid params' do
      it 'updates the requested author' do
        expect(response).to be_successful
        expect(response_json['first_name']).to eq(new_first_name)
      end

      it 'renders a JSON response with the author' do
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(response_json.keys)
          .to contain_exactly(
            'id', 'first_name', 'last_name', 'count_of_books', 'created_at', 'photo_attached?',
            'photo_filename', 'photo_url', 'updated_at'
          )
      end
    end

    context 'with invalid params' do
      let(:author_params) { { first_name: '' } }

      it 'renders a JSON response with errors for the author' do
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end
  end

  describe 'DELETE #destroy' do
    subject { delete api_v1_author_path(author.id) }

    it 'destroys the requested author' do
      expect { subject }.to change(Api::V1::Author, :count).by(-1)
    end
  end
end
