require 'rails_helper'

RSpec.describe Api::V1::BooksController, type: :request do
  let!(:book) { create :book }
  let(:book_params) { attributes_for(:book) }
  let(:params) { { book: book_params } }

  describe 'GET #index' do
    subject { get api_v1_books_path }

    it 'returns a success response' do
      subject

      expect(response).to be_successful
      expect(response_json.first.keys)
        .to contain_exactly(
          'id', 'author_id', 'title', 'description', 'price', 'rating', 'created_at',
          'updated_at', 'url'
        )
    end
  end

  describe 'GET #show' do
    subject { get api_v1_book_path(book.id) }

    it 'returns a success response' do
      subject

      expect(response).to be_successful
      expect(response_json.keys)
        .to contain_exactly(
          'id', 'author_id', 'title', 'description', 'price', 'rating', 'created_at', 'updated_at'
        )
    end
  end

  describe 'POST #create' do
    let(:author) { create :author }
    let(:new_title) { 'Completely new title' }
    let(:book_params) { { author_id: author.id, title: new_title } }
    subject { post api_v1_books_path, params: params }

    context 'with valid params' do
      it 'creates a new book' do
        expect { subject }.to change(Api::V1::Book, :count).by(1)
      end

      it 'renders a JSON response with the new book' do
        subject

        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(response.location).to eq(api_v1_book_url(Api::V1::Book.find_by(title: new_title)))
        expect(response_json.keys)
          .to contain_exactly(
            'id', 'author_id', 'title', 'description', 'price', 'rating', 'created_at', 'updated_at'
          )
      end
    end

    context 'with invalid params' do
      let(:book_params) { { title: '' } }

      it 'renders a JSON response with errors for the new book' do
        subject

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end
  end

  describe 'PUT #update' do
    let(:new_title) { 'Completely new title' }
    let(:book_params) { { title: new_title } }
    subject { put api_v1_book_path(book.id), params: params }

    before :each do
      subject
    end

    context 'with valid params' do
      it 'updates the requested book' do
        expect(response).to be_successful
        expect(response_json['title']).to eq(new_title)
      end

      it 'renders a JSON response with the book' do
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(response_json.keys)
          .to contain_exactly(
            'id', 'author_id', 'title', 'description', 'price', 'rating', 'created_at', 'updated_at'
          )
      end
    end

    context 'with invalid params' do
      let(:book_params) { { title: '' } }

      it 'renders a JSON response with errors for the book' do
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end
  end

  describe 'DELETE #destroy' do
    subject { delete api_v1_book_path(book.id) }

    it 'destroys the requested book' do
      expect { subject }.to change(Api::V1::Book, :count).by(-1)
    end
  end
end
