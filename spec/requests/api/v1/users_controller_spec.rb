require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :request do
  let!(:user) { create :user }
  let(:user_params) { attributes_for(:user) }
  let(:params) { { user: user_params } }
  let(:user_headers) { { headers: auth_headers(user) } }

  describe 'GET #index' do
    subject { get api_v1_users_path }

    it 'returns a success response' do
      subject
      expect(response).to be_successful
      expect(response_json.first.keys)
        .to contain_exactly(
          'id', 'email', 'first_name', 'last_name', 'created_at', 'updated_at', 'url'
        )
    end
  end

  describe 'GET #show' do
    subject { get api_v1_user_path(user.id) }

    it 'returns a success response' do
      subject

      expect(response).to be_successful
      expect(response_json.keys)
        .to contain_exactly(
          'id', 'email', 'first_name', 'last_name', 'created_at', 'updated_at'
        )
    end
  end

  describe 'POST #create' do
    let(:user_params) { attributes_for(:user, email: 'some@example.com') }
    subject { post api_v1_users_path, params: params }

    context 'with valid params' do
      it 'creates a new user' do
        expect { subject }.to change(Api::V1::User, :count).by(1)
      end

      it 'renders a JSON response with the new user' do
        subject

        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(response.location).to eq(api_v1_user_url(Api::V1::User.last))
        expect(response_json.keys)
          .to contain_exactly(
            'id', 'email', 'first_name', 'last_name', 'created_at', 'updated_at'
          )
      end
    end

    context 'with invalid params' do
      let(:user_params) { { email: '' } }

      it 'renders a JSON response with errors for the new user' do
        subject

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end
  end

  describe 'PUT #update' do
    let(:new_first_name) { 'Completely new first name' }
    let(:user_params) { { first_name: new_first_name } }
    subject { put api_v1_user_path(user.id), params: params }

    before :each do
      subject
    end

    context 'with valid params' do
      it 'updates the requested user' do
        expect(response).to be_successful
        expect(response_json['first_name']).to eq(new_first_name)
      end

      it 'renders a JSON response with the user' do
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(response_json.keys)
          .to contain_exactly(
            'id', 'email', 'first_name', 'last_name', 'created_at', 'updated_at'
          )
      end
    end

    context 'with invalid params' do
      let(:user_params) { { email: '' } }

      it 'renders a JSON response with errors for the user' do
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end
  end

  describe 'DELETE #destroy' do
    subject { delete api_v1_user_path(user.id) }

    it 'destroys the requested user' do
      expect { subject }.to change(Api::V1::User, :count).by(-1)
    end
  end

  describe 'GET #current' do
    subject { get current_api_v1_users_url, user_headers } # rubocop:disable Rails/HttpPositionalArguments

    it 'responds with user data' do
      subject

      expect(response_json.keys)
        .to contain_exactly('id', 'email', 'first_name', 'last_name', 'created_at', 'updated_at')
    end

    context 'user not sign in' do
      let(:user_headers) { { headers: {} } }

      it 'returns unauthorized' do
        subject
        expect(response).to have_http_status(:unauthorized)
        expect(response_json.keys)
          .to contain_exactly('error')
      end
    end
  end
end
