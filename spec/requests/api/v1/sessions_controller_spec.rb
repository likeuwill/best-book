describe Api::V1::SessionsController, type: :request do
  let(:user) { create :user }
  let(:email) { user.email }
  let(:password) { user.password }

  let(:params) do
    {
      user: {
        email: email,
        password: password
      }
    }
  end

  describe '#login' do
    context 'when params are correct' do
      before do
        post user_session_url, params: params
      end

      it 'returns 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns JTW token in authorization header' do
        expect(response.headers['Authorization']).to be_present
      end

      it 'returns valid JWT token' do
        token_from_request = response.headers['Authorization'].split(' ').last
        decoded_token = JWT.decode(token_from_request, ENV['DEVISE_JWT_SECRET_KEY'], true)
        expect(decoded_token.first['sub']).to be_present
      end
    end

    context 'when login params are incorrect' do
      before { post user_session_url }

      it 'returns unauthorized status' do
        expect(response.status).to eq 401
      end
    end

    context 'when a wrong password' do
      let(:password) { 'wrong-password' }

      before do
        post user_session_url, params: params
      end

      it 'returns unauthorized status' do
        expect(response.status).to eq 401
      end
    end

    context 'when a wrong email' do
      let(:email) { 'wrong-email@test.com' }

      before do
        post user_session_url, params: params
      end

      it 'returns unauthorized status' do
        expect(response.status).to eq 401
      end
    end
  end

  describe '#logout' do
    subject do
      delete destroy_user_session_url
    end

    before do
      post user_session_url, params: params
    end

    it 'returns 204, no content' do
      subject
      expect(response).to have_http_status(204)
      expect(response.headers['Authorization']).to be_nil
    end
  end
end
