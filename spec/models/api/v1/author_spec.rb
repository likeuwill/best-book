require 'rails_helper'

RSpec.describe Api::V1::Author, type: :model do
  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }

  context 'validate author attributes' do
    let(:author) { build(:author) }

    it 'is valid' do
      expect(author).to be_valid
    end
  end
end
