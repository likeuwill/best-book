require 'rails_helper'

RSpec.describe Api::V1::User, type: :model do
  context 'validate user attributes' do
    let(:user) { build(:user) }

    it 'is valid' do
      expect(user).to be_valid
    end
  end
end
