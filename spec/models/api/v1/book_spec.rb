require 'rails_helper'

RSpec.describe Api::V1::Book, type: :model do
  context 'validate book attributes' do
    let(:author) { create(:author) }
    let(:book) { build(:book, author: author) }

    it 'is valid' do
      expect(book).to be_valid
    end
  end

  it { should validate_inclusion_of(:rating).in_range(0..10) }
end
