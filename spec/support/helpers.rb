require 'devise/jwt/test_helpers'

module Helpers
  def response_json
    JSON.parse(response.body)
  end

  # This will add a valid token for `user` in the `Authorization` header
  def auth_headers(user)
    headers = {
      :Accept => 'application/json; charset=utf-8',
      'Content-Type' => 'application/json; charset=utf-8',
      isAdmin: user.admin
    }
    Devise::JWT::TestHelpers.auth_headers(headers, user)
  end
end
