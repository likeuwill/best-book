# rubocop:disable Style/MixinUsage
include ActionDispatch::TestProcess
# rubocop:enable Style/MixinUsage
FactoryBot.define do
  factory :author, class: Api::V1::Author do
    first_name { 'J. R. R.' }
    last_name { 'Tolkien' }
  end

  factory :author_two, class: Api::V1::Author do
    first_name { 'Christi' }
    last_name { 'Golden' }

    trait :with_photo do
      file_path = Rails.root.join('spec/support/assets/authors/authors/Christie_Golden.jpg')
      photo { fixture_file_upload(file_path, 'image/jpg') }
    end

    trait :with_books do
      after(:create) do |author|
        author.books.create(attributes_for(:book_two))
        author.books.create(attributes_for(:book_three))
      end
    end
  end
end
