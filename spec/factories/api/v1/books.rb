FactoryBot.define do # rubocop:disable Metrics/BlockLength
  factory :book, class: Api::V1::Book do
    author
    title { 'The Lord of the Rings' }
    description do
      "In ancient times the Rings of Power were crafted by the Elven-smiths, and Sauron,
      the Dark Lord, forged the One Ring, filling it with his own power so that he could
      rule all others. But the One Ring was taken from him, and though he sought it throughout
      Middle-earth, it remained lost to him. After many ages it fell by chance into the hands
      of the hobbit Bilbo Baggins."
    end
    rating { 9 }
    price { 15.99 }
  end

  factory :book_two, class: Api::V1::Book do
    association :author, factory: :author_two
    title do
      'World of Warcraft: Arthas: Rise of the Lich King'
    end
    description do
      "It was caught in a hovering, jagged chunk of ice, the runes that ran the length
      of its blade glowing a cool blue. Below it was a dais of some sort, standing on a large gently
      raised mound that was covered in a dusting of snow. A soft light, coming from somewhere high
      above where the cavern was open to daylight, shone down on the runeblade. The icy prison hid
      some details of the sword' s shape and form, exaggerated others."
    end
    rating { 8 }
    price { 11.27 }
  end

  factory :book_three, class: Api::V1::Book do
    association :author, factory: :author_two
    title { 'World of Warcraft: Thrall: Twilight of the Aspects' }
    description do
      'The Hour of Twilight. She’d spoken of it at the meeting, tried to warn the others of it,
      but the warning had gotten lost; a little bright fragment of . . . something . . .
      had been briskly swept away like a broken bit of pottery beneath an industrious broom'
    end
    rating { 9 }
    price { 11.15 }
  end
end
