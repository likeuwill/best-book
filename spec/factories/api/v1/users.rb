password = 'AdminAdmin1'

FactoryBot.define do
  factory :user, class: Api::V1::User do
    email { 'andrea.bates86@example.com' }
    first_name { 'Andrea' }
    last_name { 'Bates' }
    password { password }
    password_confirmation { password }
    jti { SecureRandom.uuid }
    admin { true }
  end
end
