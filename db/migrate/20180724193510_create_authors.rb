class CreateAuthors < ActiveRecord::Migration[5.2]
  def change
    create_table :authors, id: :uuid do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.integer :count_of_books, null: false, default: 0

      t.timestamps
    end
  end
end
