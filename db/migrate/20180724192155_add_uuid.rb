class AddUuid < ActiveRecord::Migration[5.2]
  def change
    # These are extensions that must be enabled in order to support this database
    enable_extension "uuid-ossp"
    enable_extension "pgcrypto"
  end
end
