class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books, id: :uuid do |t|
      t.uuid :author_id
      t.string :title, null: false
      t.text :description
      t.integer :price, null: false, default: 0
      t.integer :rating, null: false, default: 0

      t.timestamps
    end
  end
end
