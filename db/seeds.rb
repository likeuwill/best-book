Rails.logger = Logger.new(STDOUT)

Rails.logger.info "Deleting admin account"
Api::V1::User.find_by(email: 'andrea.bates86@example.com')&.destroy

Rails.logger.info "Creating user..."
password = 'AdminAdmin1'
Api::V1::User.create(
  email: 'andrea.bates86@example.com',
  first_name: 'Andrea',
  last_name: 'Bates',
  password: password,
  password_confirmation: password,
  jti: SecureRandom.uuid,
  admin: true
)

Rails.logger.info "Done!"
