module Api
  module V1
    class Author < ApplicationRecord
      include Rails.application.routes.url_helpers

      has_many :books,
               class_name: 'Api::V1::Book',
               inverse_of: :author,
               dependent: :restrict_with_error

      has_one_attached :photo

      validates :first_name, :last_name, presence: true
      delegate :attached?, to: :photo, prefix: true

      def photo_url
        url_for(photo) if photo.attached?
      end

      def photo_filename
        photo.filename.to_s if photo.attached?
      end
    end
  end
end
