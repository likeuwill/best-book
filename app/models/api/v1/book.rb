module Api
  module V1
    class Book < ApplicationRecord
      belongs_to :author,
                 class_name: 'Api::V1::Author',
                 inverse_of: :books,
                 optional: true,
                 counter_cache: :count_of_books

      validates :title, :author_id, presence: true
      # TODO: add database check
      validates :rating, inclusion: { in: 0..10 }
    end
  end
end
