module Api
  module V1
    class User < ApplicationRecord
      include Devise::JWT::RevocationStrategies::JTIMatcher

      devise :database_authenticatable,
             :jwt_authenticatable,
             jwt_revocation_strategy: self

      validates :email, presence: true, uniqueness: true

      def jwt_payload
        super.merge(isAdmin: admin)
      end
    end
  end
end
