module Api
  module V1
    module CustomErrorResponses
      extend ActiveSupport::Concern

      def destroy_with_errors(resource)
        if resource.destroy
          head :no_content
        else
          render json: resource.errors, status: :unprocessable_entity
        end
      end
    end
  end
end
