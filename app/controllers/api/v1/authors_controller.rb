module Api
  module V1
    class AuthorsController < ApplicationController
      before_action :set_author, only: %i[show update destroy]

      # GET /authors
      def index
        authorize Author
        @authors = Author.all

        render_jb('index')
      end

      # GET /authors/1
      def show
        authorize @author

        render_jb('show')
      end

      # POST /authors
      def create
        @author = Author.new(author_params)
        authorize @author

        if @author.save
          render_jb('show', status: :created, location: @author)
        else
          render json: @author.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /authors/1
      def update
        authorize @author

        if @author.update(author_params)
          render_jb('show')
        else
          render json: @author.errors, status: :unprocessable_entity
        end
      end

      # DELETE /authors/1
      def destroy
        authorize @author

        destroy_with_errors(@author)
      end

      private

      def set_author
        @author = Author.find(params[:id])
      end

      def author_params
        params.require(:author).permit(:first_name, :last_name, :photo)
      end
    end
  end
end
