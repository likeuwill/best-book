module Api
  module V1
    class UsersController < ApplicationController
      before_action :authenticate_user!, only: %i[current]
      before_action :set_user, only: %i[show update destroy]

      # GET /api/v1/users
      def index
        @users = Api::V1::User.all

        render_jb('index')
      end

      # GET /api/v1/users/1
      def show
        render_jb('show')
      end

      # POST /api/v1/users
      def create
        @user = Api::V1::User.new(user_params)

        if @user.save
          render_jb('show', status: :created, location: @user)
        else
          render json: @user.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /api/v1/users/1
      def update
        if @user.update(user_params)
          render_jb('show')
        else
          render json: @user.errors, status: :unprocessable_entity
        end
      end

      # DELETE /api/v1/users/1
      def destroy
        @user.destroy
      end

      # GET /api/v1/users/current
      def current
        render_jb('current') if user_signed_in?
      end

      private

      def set_user
        @user = Api::V1::User.find(params[:id])
      end

      def user_params
        params.require(:user).permit(
          :email,
          :first_name,
          :last_name,
          :string,
          :password,
          :password_confirmation
        )
      end
    end
  end
end
