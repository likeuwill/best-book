module Api
  module V1
    class BooksController < ApplicationController
      before_action :set_book, only: %i[show update destroy]

      # GET /books
      def index
        authorize Book
        @books = Book.all

        render_jb('index')
      end

      # GET /books/1
      def show
        authorize @book

        render_jb('show')
      end

      # POST /books
      def create
        @book = Book.new(book_params)
        authorize @book

        if @book.save
          render_jb('show', status: :created, location: @book)
        else
          render json: @book.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /books/1
      def update
        authorize @book

        if @book.update(book_params)
          render_jb('show')
        else
          render json: @book.errors, status: :unprocessable_entity
        end
      end

      # DELETE /books/1
      def destroy
        authorize @book

        @book.destroy
      end

      private

      def set_book
        @book = Book.find(params[:id])
      end

      def book_params
        params.require(:book).permit(:author_id, :title, :description, :price, :rating)
      end
    end
  end
end
