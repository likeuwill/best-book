module Api
  module V1
    class SystemVersionController < ApplicationController
      # GET /system_version
      def show
        render json: {
          system_version: Rails.configuration.x.current_system_version
        }
      end
    end
  end
end
