class ApplicationController < ActionController::API
  include ActionController::MimeResponds
  include Pundit
  include Api::V1::CustomErrorResponses

  def render_jb(action, status: :ok, location: nil)
    render action, status: status, location: location, handlers: 'jb'
  end

  def render_resource(resource)
    if resource.errors.empty?
      render json: resource
    else
      validation_error(resource)
    end
  end

  def validation_error(resource)
    render json: {
      errors: [
        {
          status: '400',
          title: 'Bad Request',
          detail: resource.errors,
          code: '100'
        }
      ]
    }, status: :bad_request
  end

  def fallback_index_html
    render file: 'public/index.html'
  end
end
