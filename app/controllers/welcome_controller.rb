class WelcomeController < ApplicationController
  def index
    skip_authorization

    render file: 'public/index'
  end

  def welcome
    skip_authorization

    render file: 'public/welcome'
  end
end
