module Api
  module V1
    class BookPolicy
      attr_reader :user, :book

      def initialize(user, book)
        @user = user
        @author = book
      end

      def index?
        true
      end

      def show?
        true
      end

      def create?
        true
      end

      def update?
        true
      end

      def destroy?
        true
      end
    end
  end
end
