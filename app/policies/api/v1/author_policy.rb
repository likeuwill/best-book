module Api
  module V1
    class AuthorPolicy
      attr_reader :user, :author

      def initialize(user, author)
        @user = user
        @author = author
      end

      def index?
        true
      end

      def show?
        true
      end

      def create?
        true
      end

      def update?
        true
      end

      def destroy?
        true
      end
    end
  end
end
