# Best book
Application to manage books
* https://best-book.herokuapp.com

# Tech Stacks
GitLab CI with testing and deploying api and web

## React
```
React: v.16.12
Lint
Redux
Redux-thunk
Enzyme
Jest
Flow
```

## Ruby on Rails
```
Ruby 2.6
Ruby on Rails 6
PostgreSQL 11
Devise
JWT
Pundit
Jb
AWS S3
Rspec
Rubocop
```
## React techniques
```
Context
Hooks
Lazy load
```

# Main commands

## Run tests
```
bundle exec rspec
```

## Run rubocop
```
bundle exec rubocop
```

# Run a security audit
```
bundle exec bundle-audit check --update
```

## Run api and web servers from client folder
```
rake start
```

## Run separate
API:
```
PORT=3001 bundle exec rails s
```

WEB UI:
```
cd client && PORT=3000 yarn start
```

## Kill session
```
kill -9 $(lsof -i tcp:3001 -t)
```

## Configuration (development)
Create `.env` configuration file from example config

```
cp .env.example .env
```

Enter local credentials for database:

```
nano .env
```

## Configuration GitLab CI/CD Variables
Set a list of variables (Settings->CI/CD->Variables)
```
DB_ADAPTER
DB_HOST
DB_ENCODING
DB_TEST_DATABASE_NAME
DB_TEST_USERNAME
DB_TEST_PASSWORD
HEROKU_APP_NAME
HEROKU_PRODUCTION_API_KEY
```

create-react-app bug (eslint versions conflict), should be fixed in future
https://github.com/facebook/create-react-app/issues/5247
```
SKIP_PREFLIGHT_CHECK=true
```

## Heroku
Set a list of variables
```
DB_PORT
DB_ENCODING

AWS_ACCESS_KEY_ID
AWS_BUCKET
AWS_REGION
AWS_SECRET_ACCESS_KEY
```

Push to heroku (optional, now you can merge to production branch and it will deploy automatically)
```
git push heroku master
```

Run heroku console
```
heroku run bash --app best-book
```

Restart app on heroku
```
heroku restart --app best-book
```

Enable modules cache for Heroku
```
heroku config:set NODE_MODULES_CACHE=true
```

Clear react build cache
```
heroku plugins:install heroku-repo
heroku repo:purge_cache -a best-book
```
