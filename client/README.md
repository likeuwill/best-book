# Configuration react 
First run

* Install NVM
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
```
* Install
```
nvm install node
```

* Set the latest version node
```
nvm use node
```

# Run test
```
yarn test
```

# Run ESLint
```
npx eslint --ext .js --ext .jsx src
```

# Run a security audit
```
npm audit
yarn audit
```

## Upgrades package.json dependencies to the latest versions
```
yarn global add npm-check-updates
npm install -g npm-check-updates
ncu -u
```
