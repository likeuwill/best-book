import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import App from './App';
import './index.scss';
import 'bootstrap/dist/css/bootstrap.css';
import { unregister as unregisterServiceWorker } from './registerServiceWorker';

const rootElement = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElement,
);

unregisterServiceWorker();
