/* eslint camelcase: "off" */

import axios from '../../components/App/axios-orders';
import * as actionTypes from './actionTypes';

export const fetchAuthorsFailed = () => ({
  type: actionTypes.FETCH_AUTHORS_FAILED,
});

export const fetchAuthorsStart = () => ({
  type: actionTypes.FETCH_AUTHORS_START,
});

export const fetchAuthorsSuccess = (authors) => ({
  type: actionTypes.FETCH_AUTHORS_SUCCESS,
  authors,
  error: false,
});

export const fetchAuthors = () => (dispatch) => {
  dispatch(fetchAuthorsStart());
  axios.get('authors.json')
    .then((response) => {
      setTimeout(() => {
        dispatch(fetchAuthorsSuccess(response.data));
      }, 2000);
    })
    .catch((error) => {
      dispatch(fetchAuthorsFailed());
    });
};

export const destroyAuthor = (id) => ({
  type: actionTypes.REMOVE_AUTHOR,
  id,
});

export const removeAuthorFailed = () => ({
  type: actionTypes.REMOVE_AUTHOR_FAILED,
});

export const removeAuthor = (id) => (dispatch) => {
  axios.delete(`authors/${id}`)
    .then((response) => {
      dispatch(destroyAuthor(id));
    })
    .catch((error) => {
      dispatch(removeAuthorFailed());
    });
};

export const createAuthorSuccess = (author) => ({
  type: actionTypes.CREATE_AUTHOR_SUCCESS,
  author,
});

export const createAuthorFailed = () => ({
  type: actionTypes.CREATE_AUTHOR_FAILED,
});

export const createAuthor = (first_name, last_name) => (dispatch) => {
  axios.post('authors', { author: { first_name, last_name } })
    .then((response) => {
      dispatch(createAuthorSuccess(response.data));
    })
    .catch((error) => {
      dispatch(createAuthorFailed());
    });
};

export const editAuthorSuccess = (author) => ({
  type: actionTypes.EDIT_AUTHOR_SUCCESS,
  author,
});

export const editAuthorFailed = () => ({
  type: actionTypes.EDIT_AUTHOR_FAILED,
});

export const editAuthor = (id, formAuthorData) => (dispatch) => {
  axios.put(`authors/${id}`,
    formAuthorData, { headers: { 'Content-Type': 'multipart/form-data' } }).then((response) => {
    dispatch(editAuthorSuccess(response.data));
  })
    .catch((error) => {
      dispatch(editAuthorFailed());
    });
};
