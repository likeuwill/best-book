import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
  authors: [],
  loading: false,
  error: false,
};

const fetchAuthorsStart = (state) => updateObject(state, { loading: true });

const fetchAuthorsSuccess = (state, action) => updateObject(state, {
  loading: false,
  authors: action.authors,
});

const createAuthor = (state, action) => updateObject(state, {
  loading: false,
  authors: [...state.authors, action.author],
});

const editAuthor = (state, action) => {
  const updatedAuthors = state.authors.map((author) => {
    if (author.id === action.author.id) {
      return { ...author, ...action.author };
    }
    return author;
  });

  return updateObject(state, { authors: updatedAuthors });
};

const removeAuthor = (state, action) => {
  const updatedAuthors = state.authors.filter(
    (author) => author.id !== action.id,
  );

  return updateObject(state, { authors: updatedAuthors });
};

const actionFailed = (state) => updateObject(state, { loading: false, error: true });

const reducerAuthor = (state = initialState, action) => {
  switch (action.type) {
  case actionTypes.FETCH_AUTHORS_START: return fetchAuthorsStart(state, action);
  case actionTypes.FETCH_AUTHORS_SUCCESS: return fetchAuthorsSuccess(state, action);
  case actionTypes.FETCH_AUTHORS_FAILED: return actionFailed(state, action);
  case actionTypes.CREATE_AUTHOR_SUCCESS: return createAuthor(state, action);
  case actionTypes.CREATE_AUTHOR_FAILED: return actionFailed(state, action);
  case actionTypes.EDIT_AUTHOR_SUCCESS: return editAuthor(state, action);
  case actionTypes.EDIT_AUTHOR_FAILED: return actionFailed(state, action);
  case actionTypes.REMOVE_AUTHOR: return removeAuthor(state, action);
  case actionTypes.REMOVE_AUTHOR_FAILED: return actionFailed(state, action);
  default: return state;
  }
};

export default reducerAuthor;
