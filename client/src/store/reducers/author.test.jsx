import reducer from './author';

describe('author reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      authors: [],
      loading: false,
      error: false,
    });
  });
});
