import { combineReducers } from 'redux';
import reducerAuthor from './author';

const reducers = combineReducers({
  author: reducerAuthor,
});

export default reducers;
