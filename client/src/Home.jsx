import React from 'react';
import './App.scss';
import Header from './components/Header';
import NavBar from './components/NavBar/NavBar';
import Footer from './components/Footer';
import AuthorsContainer from './components/Author/AuthorsContainer';
import { AuthProvider } from './components/authContext';
import WelcomeMsg from './components/WelcomeMsg';

function Home() {
  return (
    <div className="App">
      <AuthProvider>
        <NavBar />
        <WelcomeMsg />
        <Header />
        <AuthorsContainer />
        <Footer />
      </AuthProvider>
    </div>
  );
}

export default Home;
