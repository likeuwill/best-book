import { Component } from 'react';
import routes from './components/App/routes';

class App extends Component {
  render() {
    return routes;
  }
}

export default App;
