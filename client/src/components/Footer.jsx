import React from 'react';
import { CardFooter } from 'reactstrap';
import SystemVersion from './SystemVersion';

export default function Footer() {
  return (
    <CardFooter className="App-footer">
      © 2020 Best Book
      <SystemVersion />
    </CardFooter>
  );
}
