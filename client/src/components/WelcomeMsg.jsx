import React, { Component } from 'react';
import { Alert } from 'reactstrap';
import { AuthContext } from './authContext';

export default class WelcomeMsg extends Component {
  static contextType = AuthContext;

  state = {
    visible: true,
  };

  onDismiss = () => {
    this.setState({ visible: false });
    this.context.disableWelcomeMsg();
  }

  alertMessage = (messageText) => (
    <Alert color="primary" isOpen={this.state.visible} toggle={this.onDismiss} fade>
      {messageText}
    </Alert>
  )

  render() {
    const adminMsg = this.context.isAdmin ? 'You are admin.' : 'You are not admin.';
    const welcomeMsg = this.context.isLoggedIn ? (
      this.alertMessage(`Hello ${this.context.user.first_name}! ${adminMsg}`)
    ) : (
      this.alertMessage('Hello Guest!')
    );
    const showWelcomeMsg = this.context.welcomeMsgIsShowed ? null : welcomeMsg;

    return (
      <div className="welcome-msg">
        {showWelcomeMsg}
      </div>
    );
  }
}
