import React, { Component } from 'react';
import axios from './App/axios-orders';

class SystemVersion extends Component {
  state = {
    current_version: '',
  }

  componentDidMount() {
    axios.get('system_version.json')
      .then((response) => {
        this.setState({
          current_version: response.data.system_version,
        });
      })
      .catch((error) => console.log(error.response));
  }

  render() {
    return (
      <>
        <p>
          version:
          {' '}
          {this.state.current_version}
        </p>
      </>
    );
  }
}

export default SystemVersion;
