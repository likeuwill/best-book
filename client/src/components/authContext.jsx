import React, { Component } from 'react';
import PropTypes from 'prop-types';
import jwt from 'jsonwebtoken';

const AuthContext = React.createContext();

export class AuthProvider extends Component {
  state = {
    isLoggedIn: false,
    user: {},
    isAdmin: false,
    welcomeMsgIsShowed: false,
  }

  componentDidMount() {
    this.hydrateStateWithLocalStorage();
  }

  logIn = (user, token) => {
    const decodedToken = jwt.decode(token);
    this.setState({
      isLoggedIn: true,
      user,
      isAdmin: decodedToken.isAdmin,
    });
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
  };

  logOut = () => {
    this.setState({
      isLoggedIn: false,
      user: {},
      isAdmin: false,
      welcomeMsgIsShowed: false,
    });
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    localStorage.removeItem('welcomeMsgIsShowed');
  };

  disableWelcomeMsg = () => {
    this.setState({
      welcomeMsgIsShowed: true,
    });
    localStorage.setItem('welcomeMsgIsShowed', '');
  };

  hydrateStateWithLocalStorage = () => {
    if (hasOwnProperty.call(localStorage, 'user')) {
      // get the user value from localStorage
      let user = localStorage.getItem('user');

      // parse the localStorage object and setState
      try {
        user = JSON.parse(user);
        this.setState({
          isLoggedIn: true,
          user,
          isAdmin: user.admin,
        });
      } catch (e) {
        // handle error
      }
    }
    if (hasOwnProperty.call(localStorage, 'welcomeMsgIsShowed')) {
      this.setState({
        welcomeMsgIsShowed: true,
      });
    }
  };

  render() {
    return (
      <AuthContext.Provider
        value={{
          logIn: this.logIn,
          logOut: this.logOut,
          isLoggedIn: this.state.isLoggedIn,
          user: this.state.user,
          isAdmin: this.state.isAdmin,
          disableWelcomeMsg: this.disableWelcomeMsg,
          welcomeMsgIsShowed: this.state.welcomeMsgIsShowed,
        }}
      >
        {this.props.children}
      </AuthContext.Provider>
    );
  }
}

export { AuthContext };

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
