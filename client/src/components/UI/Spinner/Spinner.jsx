import React from 'react';
import PropTypes from 'prop-types';
import { PropagateLoader } from 'react-spinners';
import './spinner.scss';

const spinner = (props) => (
  <>
    Loading...
    <div className="sweet-loading">
      <PropagateLoader
        sizeUnit="px"
        size={20}
        margin={200}
        color="#194d33"
        loading={props.loading}
      />
    </div>
  </>
);

export default spinner;

// PROPERTIES TYPES
spinner.propTypes = {
  loading: PropTypes.bool,
};

spinner.defaultProps = {
  loading: true,
};
