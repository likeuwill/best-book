import React, { Component } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem, Button, Modal, ModalHeader, ModalBody,
} from 'reactstrap';
import { FaHome } from 'react-icons/fa';
import Login from '../Login/Login';
import { AuthContext } from '../authContext';
import axios from '../App/axios-orders';

class NavBar extends Component {
  static contextType = AuthContext;

  state = {
    menuIsOpen: false,
    loginIsOpen: false,
  };

  toggleModal = () => {
    this.setState((prevState) => ({ menuIsOpen: !prevState.menuIsOpen }));
  }

  toggleLoginModal = () => {
    this.setState((prevState) => ({ loginIsOpen: !prevState.loginIsOpen }));
  }

  logOut = () => {
    axios.delete('/users/sign_out')
      .then(() => {
        this.context.logOut();
      })
      .catch((error) => {
        console.log(error.response);
      });
  }

  render() {
    const closeBtn = <Button className="close" onClick={this.toggleLoginModal}>&times;</Button>;
    const loginModal = (
      <Modal
        isOpen={this.state.loginIsOpen}
        toggle={this.toggleLoginModal}
      >
        <ModalHeader toggle={this.toggleLoginModal} close={closeBtn}>
          Sign in
        </ModalHeader>
        <ModalBody>
          <Login
            toggle={this.toggleLoginModal}
          />
        </ModalBody>
      </Modal>
    );

    return (
      <>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/"><FaHome size={28} color="#186c5a" /></NavbarBrand>
          <NavbarToggler onClick={this.toggleModal} />
          <Collapse isOpen={this.state.menuIsOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="/authors">Authors</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/books">Books</NavLink>
              </NavItem>
              <NavItem>
                <NavLink disabled href="/users">Users</NavLink>
              </NavItem>
              {this.context.isLoggedIn ? (
                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle nav caret>
                    Profile
                  </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem>
                      <NavLink disabled href="/settings">Settings</NavLink>
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>
                      <NavLink
                        onClick={() => this.logOut()}
                      >
                        Log out
                      </NavLink>
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              ) : (
                <NavLink
                  onClick={() => this.toggleLoginModal()}
                >
                  Sign in
                </NavLink>
              )}
            </Nav>
          </Collapse>
        </Navbar>
        {loginModal}
      </>
    );
  }
}

export default NavBar;
