import React, { Component, Suspense } from 'react';
import { connect } from 'react-redux';
import {
  Button, Table, Modal, ModalBody, ModalHeader,
} from 'reactstrap';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import './authors.scss';
import PropTypes from 'prop-types';
import Authors from './Authors';
import NewAuthorForm from './NewAuthorForm';
import * as bindActionCreators from '../../store/actions';
import Spinner from '../UI/Spinner/Spinner';

const LazyEditAuthorForm = React.lazy(() => import('./EditAuthorForm'));

class AuthorsContainer extends Component {
  state = {
    authorObject: null,
    modalIsOpen: false,
    imageIsOpen: false,
  };

  componentDidMount() {
    this.props.onInitAuthors();
  }

  showModalWithAuthor = (author) => {
    this.setState({ authorObject: author });
    this.toggleModal();
  }

  toggleModal = () => {
    this.setState((prevState) => ({ modalIsOpen: !prevState.modalIsOpen }));
  }

  showPopupImage = (author) => {
    this.setState({ authorObject: author });
    this.toggleImage();
  }

  toggleImage = () => {
    this.setState((prevState) => ({ imageIsOpen: !prevState.imageIsOpen }));
  }

  render() {
    const closeBtn = <Button className="close" onClick={this.toggleModal}>&times;</Button>;
    const modalWindow = (
      <Modal
        isOpen={this.state.modalIsOpen}
        toggle={this.toggleModal}
        className={this.props.className}
      >
        <ModalHeader toggle={this.toggleModal} close={closeBtn}>
          Edit Author
        </ModalHeader>
        <ModalBody>
          <Suspense fallback={<div>Loading...</div>}>
            <LazyEditAuthorForm
              author={this.state.authorObject}
              onEditAuthor={this.props.onEditAuthor}
              toggle={this.toggleModal}
            />
          </Suspense>
        </ModalBody>
      </Modal>
    );
    return (
      <>
        <div className="table-responsive">
          <div className="authors-container">
            <NewAuthorForm onCreateAuthor={this.props.onCreateAuthor} />
            <Table hover>
              <thead>
                <tr>
                  <th>Photo</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Count of books</th>
                  <th />
                  <th />
                  <th />
                </tr>
              </thead>
              <tbody>
                {!this.props.loading ? (
                  <Authors
                    athrs={this.props.athrs}
                    showPopupImage={this.showPopupImage}
                    showModalWithAuthor={this.showModalWithAuthor}
                    onRemoveAuthor={this.props.onRemoveAuthor}
                  />
                ) : (
                  <tr>
                    <td colSpan={7}>
                      {this.props.loading && <Spinner loading={this.state.loading} />}
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        </div>
        {modalWindow}
        {this.state.imageIsOpen && (
          <Lightbox
            mainSrc={this.state.authorObject.photo_url}
            onCloseRequest={this.toggleImage}
          />
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  athrs: state.author.authors,
  loading: state.author.loading,
});

const mapDispatchToProps = (dispatch) => ({
  onInitAuthors: () => dispatch(bindActionCreators.fetchAuthors()),
  onCreateAuthor: (firstName, lastName) => dispatch(bindActionCreators.createAuthor(
    firstName, lastName,
  )),
  onEditAuthor: (id, formAuthorData) => dispatch(bindActionCreators.editAuthor(
    id, formAuthorData,
  )),
  onRemoveAuthor: (id) => dispatch(bindActionCreators.removeAuthor(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthorsContainer);

// PROPERTIES TYPES
AuthorsContainer.propTypes = {
  onInitAuthors: PropTypes.func.isRequired,
  onCreateAuthor: PropTypes.func.isRequired,
  onRemoveAuthor: PropTypes.func.isRequired,
  onEditAuthor: PropTypes.func.isRequired,
  athrs: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    first_name: PropTypes.string,
    last_name: PropTypes.string,
    photo_url: PropTypes.string,
  })).isRequired,
  className: PropTypes.string,
  loading: PropTypes.bool,
};

AuthorsContainer.defaultProps = {
  className: null,
  loading: false,
};
