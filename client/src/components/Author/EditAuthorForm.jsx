import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button, ModalFooter, Card, CardBody,
} from 'reactstrap';
import { Img } from 'react-image';
import Uploader from '../App/Uploader/Uploader';
import * as Constants from '../App/constants';

class EditAuthorForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editableAuthor: props.author,
      selectedImage: null,
    };
  }

  handleInputChange = ({ target }) => {
    const { value, name } = target;
    const { editableAuthor } = this.state;

    this.setState({
      editableAuthor: { ...editableAuthor, [name]: value },
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const { editableAuthor, selectedImage } = this.state;

    const formAuthorData = new FormData();
    formAuthorData.append('author[first_name]', editableAuthor.first_name);
    formAuthorData.append('author[last_name]', editableAuthor.last_name);

    if (selectedImage !== null) {
      formAuthorData.append('author[photo]', selectedImage, selectedImage.filename);
    }

    this.props.onEditAuthor(
      this.state.editableAuthor.id,
      formAuthorData,
    );
    this.props.toggle();
  };

  addImage = (selectedImage) => {
    this.setState({
      selectedImage,
    });
  }

  onCancel =() => {
    this.setState({
      selectedImage: null,
    });
    this.props.toggle();
  }

  render() {
    const { editableAuthor } = this.state;

    return (
      <div className="edit-author">
        <Card>
          <CardBody>
            <div className="edit-author-images">
              <Img
                src={this.state.editableAuthor.photo_url}
                alt="photo_url"
                unloader={<Img src={Constants.NO_IMAGE} />}
              />
            </div>
            <form onSubmit={this.handleSubmit}>
              First name
              <p>
                <input
                  type="text"
                  name="first_name"
                  id="first_name"
                  placeholder="First name..."
                  value={editableAuthor.first_name}
                  onChange={this.handleInputChange}
                />
              </p>
              Last name
              <p>
                <input
                  type="text"
                  name="last_name"
                  id="last_name"
                  placeholder="Last name..."
                  value={editableAuthor.last_name}
                  onChange={this.handleInputChange}
                />
              </p>
              <Uploader
                id={this.state.editableAuthor.id}
                addImage={this.addImage}
              />
              <ModalFooter>
                <Button color="primary">Update Author</Button>
                {' '}
                <Button
                  onClick={this.onCancel}
                  color="secondary"
                >
                  Cancel
                </Button>
              </ModalFooter>
            </form>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default EditAuthorForm;

// PROPERTIES TYPES
EditAuthorForm.propTypes = {
  author: PropTypes.shape({
    id: PropTypes.string.isRequired,
    first_name: PropTypes.string,
    last_name: PropTypes.string,
    photo_url: PropTypes.string,
  }),
  toggle: PropTypes.func.isRequired,
  onEditAuthor: PropTypes.func.isRequired,
};

EditAuthorForm.defaultProps = {
  author: null,
};
