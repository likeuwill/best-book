import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';

const NewAuthorForm = (props) => {
  let firstName;
  let lastName;

  const submit = (e) => {
    e.preventDefault();
    props.onCreateAuthor(firstName.value, lastName.value);
    firstName.focus();
    firstName.value = '';
    lastName.value = '';
  };

  return (
    <div className="new-author">
      <form onSubmit={submit}>
        <input
          ref={(input) => { firstName = input; }}
          type="text"
          placeholder="First name..."
          required
        />
        <input
          ref={(input) => { lastName = input; }}
          type="text"
          placeholder="Last name..."
          required
        />
        <Button>
          Add Author
        </Button>
      </form>
    </div>
  );
};

export default NewAuthorForm;

// PROPERTIES TYPES
NewAuthorForm.propTypes = {
  onCreateAuthor: PropTypes.func.isRequired,
};
