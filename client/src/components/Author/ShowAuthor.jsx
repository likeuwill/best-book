import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './authors.scss';
import { Img } from 'react-image';
import axios from '../App/axios-orders';
import * as Constants from '../App/constants';

class ShowAuthor extends Component {
  state = {
    author: '',
  };

  // TODO: use redux
  componentDidMount() {
    axios.get(`authors/${this.props.match.params.id}`)
      .then((response) => {
        this.setState({
          author: response.data,
        });
      })
      .catch((error) => console.log(error.response));
  }

  render() {
    return (
      this.state.author ? (
        <div className="single-author">
          <Img
            src={this.state.author.photo_url}
            alt="photo_url"
            unloader={<Img src={Constants.NO_IMAGE} />}
          />
          <p>
            <b>First name:</b>
            {' '}
            {this.state.author.first_name}
          </p>
          <p>
            <b>Last name:</b>
            {' '}
            {this.state.author.last_name}
          </p>
          <p>
            <b>Count of books:</b>
            {' '}
            {this.state.author.count_of_books}
          </p>
        </div>

      ) : (
        'Loading author data...'
      )
    );
  }
}

export default ShowAuthor;

// PROPERTIES TYPES
ShowAuthor.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }).isRequired,
};
