import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { Img } from 'react-image';
import * as Constants from '../App/constants';

const athrs = (props) => props.athrs.map((author) => (
  <tr key={author.id}>
    <th scope="row" className="photo-table">
      <Img
        src={author.photo_url}
        alt="photo_url"
        unloader={<Img src={Constants.NO_IMAGE} />}
        onClick={() => props.showPopupImage(author)}
        style={{ cursor: 'pointer' }}
      />
    </th>
    <td>{author.first_name}</td>
    <td>{author.last_name}</td>
    <td>{author.count_of_books}</td>
    <td>
      <Link
        className="btn btn-info"
        to={`/authors/${author.id}`}
      >
        Show
      </Link>
    </td>
    <td>
      <Button
        tag="a"
        color="info"
        size="large"
        onClick={() => props.showModalWithAuthor(author)}
      >
        Edit
      </Button>
    </td>
    <td>
      <Button
        tag="a"
        color="danger"
        size="large"
        onClick={() => props.onRemoveAuthor(author.id)}
      >
        Delete
      </Button>
    </td>
  </tr>
));

export default athrs;
