import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { Img } from 'react-image';
import * as Constants from '../App/constants';

class Author extends Component {
  handleClick = () => {
    this.props.editingAuthor(this.props.author.id);
    this.props.toggle();
  }

  render() {
    return (
      <tbody>
        <tr>
          <th scope="row" className="photo-table">
            <Img
              src={this.props.author.photo_url}
              alt="photo_url"
              unloader={<Img src={Constants.NO_IMAGE} />}
            />
          </th>
          <td>{this.props.author.first_name}</td>
          <td>{this.props.author.last_name}</td>
          <td>{this.props.author.count_of_books}</td>
          <td>
            <Link
              className="btn btn-info"
              color="primary"
              to={`/authors/${this.props.author.id}`}
            >
              Show
            </Link>
          </td>
          <td>
            <Button
              tag="a"
              color="info"
              size="large"
              onClick={this.handleClick}
            >
              Edit
            </Button>
          </td>
          <td>
            <Button
              tag="a"
              color="danger"
              size="large"
              onClick={() => this.props.onRemoveAuthor(this.props.author.id)}
            >
              Delete
            </Button>
          </td>
        </tr>
      </tbody>
    );
  }
}

export default Author;

// PROPERTIES TYPES
Author.propTypes = {
  author: PropTypes.shape({
    id: PropTypes.string.isRequired,
    first_name: PropTypes.string,
    last_name: PropTypes.string,
    photo_url: PropTypes.string,
    count_of_books: PropTypes.number,
  }).isRequired,
  onRemoveAuthor: PropTypes.func.isRequired,
  editingAuthor: PropTypes.func.isRequired,
  toggle: PropTypes.func.isRequired,
};
