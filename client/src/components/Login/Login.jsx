import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Container, Col, Form,
  FormGroup, Label, Input,
  Button, FormText, FormFeedback,
} from 'reactstrap';
import './Login.css';
import axios from '../App/axios-orders';
import { AuthContext } from '../authContext';

export default class Login extends Component {
  static contextType = AuthContext;

  // TO DO: Use empty strings for default values
  state = {
    email: 'andrea.bates86@example.com',
    password: 'AdminAdmin1',
    validate: {
      emailState: '',
    },
  };

  handleChange = async (event) => {
    const { target } = event;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const { name } = target;
    await this.setState({
      [name]: value,
    });
  }

  validateEmail(e) {
    const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const { validate } = this.state;
    if (emailRex.test(e.target.value)) {
      validate.emailState = 'has-success';
    } else {
      validate.emailState = 'has-danger';
    }
    this.setState({ validate });
  }

  submitForm(e) {
    e.preventDefault();
    const { email, password } = this.state;
    axios.post('/users/sign_in', { user: { email, password } })
      .then((response) => {
        const user = response.data;
        const token = response.headers.authorization.split('Bearer ')[1];
        this.context.logIn(user, token);
        this.props.toggle();
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    const { email, password } = this.state;
    return (
      <Container className="Login">
        <Form className="form" onSubmit={(e) => this.submitForm(e)}>
          <Col>
            <FormGroup>
              <Label>Username</Label>
              <Input
                type="email"
                name="email"
                id="exampleEmail"
                placeholder="myemail@email.com"
                value={email}
                valid={this.state.validate.emailState === 'has-success'}
                invalid={this.state.validate.emailState === 'has-danger'}
                onChange={(e) => {
                  this.validateEmail(e);
                  this.handleChange(e);
                }}
              />
              <FormFeedback valid>
                That&apos;s a tasty looking email you&apos;ve got there.
              </FormFeedback>
              <FormFeedback>
                Uh oh! Looks like there is an issue with your email. Please input a correct email.
              </FormFeedback>
              <FormText>Your username is most likely your email.</FormText>
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label for="examplePassword">Password</Label>
              <Input
                type="password"
                name="password"
                id="examplePassword"
                placeholder="********"
                value={password}
                onChange={(e) => this.handleChange(e)}
              />
            </FormGroup>
          </Col>
          <Button>Submit</Button>
        </Form>
      </Container>
    );
  }
}

// PROPERTIES TYPES
Login.propTypes = {
  toggle: PropTypes.func.isRequired,
};
