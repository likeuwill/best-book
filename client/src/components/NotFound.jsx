import React from 'react';
import { Button, Container } from 'reactstrap';
import { Link } from 'react-router-dom';
import './notfound.scss';

export default function NotFound() {
  return (
    <Container className="notfound">
      <h1>404: Not found</h1>
      <p><Button tag={Link} to="/">Back to home page</Button></p>
    </Container>
  );
}
