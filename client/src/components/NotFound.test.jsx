import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import NotFound from './NotFound';

configure({ adapter: new Adapter() });

describe('<NotFound />', () => {
  it('should render <NotFound /> elements if page not founds', () => {
    const wrapper = shallow(<NotFound />);
    expect(wrapper.find('.notfound')).toHaveLength(1);
  });
});
