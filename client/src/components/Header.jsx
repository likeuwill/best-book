import React from 'react';
import logo from '../logo.png';

export default function Header() {
  return (
    <header className="header">
      <div className="logo-box">
        <img src={logo} className="logo" alt="logo" />
      </div>
      <div className="text-box">
        <h1 className="heading-primary">
          <span className="heading-primary-main">
            Your Best Book
          </span>
          <span className="heading-primary-sub">
            So many books, so little time
          </span>
        </h1>
      </div>
    </header>
  );
}
