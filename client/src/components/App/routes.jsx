import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from '../../Home';
import AuthorsContainer from '../Author/AuthorsContainer';
import ShowAuthor from '../Author/ShowAuthor';
import Login from '../Login/Login';
import NotFound from '../NotFound';

// Define routing for containers
export default (
  <Router>
    <Switch>
      <Route path="/" exact component={Home} />

      {/* LOGIN MENU */}
      <Route path="/login" exact component={Login} />

      {/* AUTHORS */}
      <Route exact path="/authors" component={AuthorsContainer} />
      <Route path="/authors/:id" component={ShowAuthor} />

      {/* NOT FOUND */}
      <Route component={NotFound} />
    </Switch>
  </Router>
);
