import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import { FaDownload } from 'react-icons/fa';
import * as Constants from './constants';
import './uploader.scss';

class Uploader extends React.Component {
  constructor(author) {
    super();
    this.state = {
      images: [],
      id: author.id,
    };
  }

  componentWillUnmount() {
    // Make sure to revoke the data uris to avoid memory leaks
    const { images } = this.state;

    if (images && images.length > 0) {
      for (let i = images.length; i >= 0; i -= 1) {
        const file = images[0];
        URL.revokeObjectURL(file.preview);
      }
    }
  }

  onFormSubmit = (acceptedFiles) => {
    const selectedImage = acceptedFiles[0];
    this.props.addImage(selectedImage);
  }

  onDrop = (acceptedFiles) => {
    this.setState({
      images: acceptedFiles.map((file) => ({
        ...file,
        preview: URL.createObjectURL(file),
      })),
    });

    this.onFormSubmit(acceptedFiles);
  }

  onCancel = () => {
    this.setState({ images: [] });
  }

  render() {
    const { images } = this.state;

    const showPreview = images.map((file) => (
      <div key={this.state.id} style={Constants.THUMB}>
        <div style={Constants.HUMBINNER}>
          <img
            src={file.preview}
            style={Constants.IMG}
            alt="preview"
          />
        </div>
      </div>
    ));

    return (
      <>
        <section className="dropzone--selection">
          <Dropzone
            onDrop={this.onDrop}
            onFileDialogCancel={this.onCancel}
            className="dropzone"
            accept="image/*"
            multiple={false}
          >
            {({
              getRootProps, getInputProps,
            }) => (
              <div {...getRootProps()} className="dropzone">
                <input {...getInputProps()} />
                <div>
                  <FaDownload size={54} color="#186c5a" cursor="pointer" />
                  <p>Drag and drop or click here</p>
                </div>
              </div>
            )}
          </Dropzone>
        </section>
        {showPreview}
      </>
    );
  }
}

export default Uploader;

// PROPERTIES TYPES
Uploader.propTypes = {
  addImage: PropTypes.func.isRequired,
};
