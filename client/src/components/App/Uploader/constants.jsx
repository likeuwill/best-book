export const THUMB = {
  display: 'inline-flex',
  borderRadius: 2,
  border: '1px solid #eaeaea',
  padding: 4,
  boxSizing: 'border-box',
};

export const HUMBINNER = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden',
};

export const IMG = {
  display: 'block',
  width: 'none',
  height: '100%',
};
