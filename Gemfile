source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

ruby '2.7.1'

gem 'aws-sdk-s3', require: false
gem 'devise'
gem 'devise-jwt'
gem 'jb'
gem 'pg'
gem 'puma'
gem 'pundit'
gem 'rack-cors', require: 'rack/cors'
gem 'rails', '~> 6.0.3.3'

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'spring'
end

group :development, :test do
  gem 'byebug'
  gem 'cucumber-rails', require: false
  gem 'dotenv-rails'
  gem 'factory_bot_rails'
  gem 'foreman'
  gem 'rspec-rails'
  gem 'simplecov', require: false
end

group :test do
  gem 'bundler-audit'
  gem 'capybara'
  gem 'database_cleaner'
  gem 'faker'
  gem 'guard-rspec'
  gem 'launchy'
  gem 'rubocop-rails'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers'
  gem 'webdrivers'
end
